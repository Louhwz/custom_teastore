REGISTRY=registry.cn-hangzhou.aliyuncs.com/louhwz

TAG=0.0.14

build-cart:
	GOOS=linux CGO_ENABLED=0 go build -o ./cart/main ./cart
	docker build -t $(REGISTRY)/cart:$(TAG) -f ./cart/Dockerfile ./cart
	rm -rf ./cart/main
.PHONY: build-cart

build-catalog:
	GOOS=linux CGO_ENABLED=0 go build -o ./catalog/main ./catalog
	docker build -t $(REGISTRY)/catalog:$(TAG) -f ./catalog/Dockerfile ./catalog
	rm -rf ./catalog/main
.PHONY: build-catalog

build-detail:
	GOOS=linux CGO_ENABLED=0 go build -o ./detail/main ./detail
	docker build -t $(REGISTRY)/detail:$(TAG) -f ./detail/Dockerfile ./detail
	rm -rf ./detail/main
.PHONY: build-detail

build-payment:
	GOOS=linux CGO_ENABLED=0 go build -o ./payment/main ./payment
	docker build -t $(REGISTRY)/payment:$(TAG) -f ./payment/Dockerfile ./payment
	rm -rf ./payment/main
.PHONY: build-payment

build-recommend:
	GOOS=linux CGO_ENABLED=0 go build -o ./recommend/main ./recommend
	docker build -t $(REGISTRY)/recommend:$(TAG) -f ./recommend/Dockerfile ./recommend
	rm -rf ./recommend/main
.PHONY: build-recommend

build-webui:
	GOOS=linux CGO_ENABLED=0 go build -o ./webui/main ./webui
	docker build -t $(REGISTRY)/webui:$(TAG) -f ./webui/Dockerfile ./webui
	rm -rf ./webui/main
.PHONY: build-webui

build-all: build-cart build-catalog build-detail build-payment build-recommend build-webui
.PHONY: build-all

push-all: build-all
	docker push $(REGISTRY)/cart:$(TAG)
	docker push $(REGISTRY)/catalog:$(TAG)
	docker push $(REGISTRY)/detail:$(TAG)
	docker push $(REGISTRY)/payment:$(TAG)
	docker push $(REGISTRY)/recommend:$(TAG)
	docker push $(REGISTRY)/webui:$(TAG)
.PHONY: push-all

sed:
	sed -i "s/0.0.12/0.0.14/g" `grep 0.0.12 -rl ./kubernetes`
.PHONY: sed

deploy-all:
	kubectl apply -f ./kubernetes -n robot-shop

deploy-oneline: push-all sed deploy-all

KAFKA-TAG=1.0.5

build-kafka-webui:
	GOOS=linux CGO_ENABLED=0 go build -o ./webui/main ./webui
	docker build -t $(REGISTRY)/webui:$(KAFKA-TAG) -f ./webui/Dockerfile ./webui
	rm -rf ./webui/main
.PHONY: build-kafka-webui

build-kafka-catalog:
	GOOS=linux CGO_ENABLED=0 go build -o ./catalog/main ./catalog
	docker build -t $(REGISTRY)/catalog:$(KAFKA-TAG) -f ./catalog/Dockerfile ./catalog
	rm -rf ./catalog/main
.PHONY: build-kafka-catalog

build-kafka-all: build-kafka-webui build-kafka-catalog
.PHONY: build-kafka-all

push-kafka-all: build-kafka-all
	docker push $(REGISTRY)/webui:$(KAFKA-TAG)
	docker push $(REGISTRY)/catalog:$(KAFKA-TAG)
.PHONY: push-kafka-all

sed-kafka:
	sed -i "s/1.0.4/1.0.5/g" `grep 1.0.4 -rl ./kubernetes/kafka-version`
.PHONY: sed-kafka

deploy-kafka-all:
	kubectl apply -f ./kubernetes/kafka-version -n kafka-teastore
.PHONY: deploy-kafka-all

deploy-kafka-oneline: build-kafka-all push-kafka-all sed-kafka deploy-kafka-all