package main

import (
	"context"
	"time"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/sirupsen/logrus"
)

type kafka_server struct{}

func (k kafka_server) kafka_c1_webui_catalog() {
	for {
		now := time.Now()
		m, err := reader_c1_webui_catalog.ReadMessage(context.Background())
		if err != nil {
			logrus.Error(err)
		}
		logrus.Infof("message spend=%v, time=%v, at offset %d: %s = %s\n", time.Since(now), time.Now().UnixNano(), m.Offset, string(m.Key), string(m.Value))

		err = pkg.IO(pkg.IOBIG, false)
		if err != nil {
			logrus.Error(err)
			pkg.WriterMessage(writer_c1_catalog_webui, string(m.Key), err.Error())
			continue
		}
		pkg.Memory(pkg.MemoryMedium)
		pkg.PI(pkg.SmallLength)

		now = time.Now()
		pkg.WriterMessage(writer_c1_catalog_webui, string(m.Key), pkg.GenerateString())
		logrus.Infof("write msg spend=%v. now=%v", time.Since(now), time.Now().UnixNano())
	}
}

func (k kafka_server) kafka_c2_webui_catalog() {
	for {
		now := time.Now()
		m, err := reader_c1_webui_catalog.ReadMessage(context.Background())
		if err != nil {
			logrus.Error(err)
		}
		logrus.Infof("message time=%v, at offset %d: %s = %s\n", time.Now().UnixNano(), m.Offset, string(m.Key), string(m.Value))

		err = pkg.IO(pkg.IOBIG, false)
		if err != nil {
			logrus.Error(err)
			pkg.WriterMessage(writer_c1_catalog_webui, string(m.Key), err.Error())
			continue
		}
		pkg.Memory(pkg.MemoryMedium)
		pkg.PI(pkg.SmallLength)

		now = time.Now()
		pkg.WriterMessage(writer_c1_catalog_webui, string(m.Key), pkg.GenerateString())
		logrus.Infof("write msg spend=%v", time.Since(now))
	}
}
