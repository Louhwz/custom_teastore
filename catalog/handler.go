package main

import (
	"net/http"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type server struct {
}

func (s server) PaymentHandler(c *gin.Context) {
	err := pkg.IO(pkg.IOBIG, false)
	if err != nil {
		logrus.Error(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	pkg.Memory(pkg.MemoryMedium)
	pkg.PI(pkg.SmallLength)
	c.String(http.StatusOK, pkg.GenerateString())
	// resp, err := http.Get(pkg.URL(pkg.PAYMENT, "api/payment"))
	// pkg.FillResp(c, resp, err)
}

func (s server) DetailPaymentHandler(c *gin.Context) {
	err := pkg.IO(pkg.IOBIG, false)
	if err != nil {
		logrus.Error(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	pkg.Memory(pkg.MemoryMedium)

	resp, err := http.Get(pkg.URL(pkg.DETAIL, "api/payment"))
	pkg.FillResp(c, resp, err)
}

func (s server) DetailRecommendHandler(c *gin.Context) {
	err := pkg.IO(pkg.IOSMALL, false)
	if err != nil {
		logrus.Error(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	pkg.Memory(pkg.MemoryMedium)

	resp, err := http.Get(pkg.URL(pkg.DETAIL, "api/recommend"))
	pkg.FillResp(c, resp, err)
}
