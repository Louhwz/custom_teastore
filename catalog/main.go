package main

import (
	"os"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

var (
	reader_c1_webui_catalog *kafka.Reader
	writer_c1_catalog_webui *kafka.Writer
)

func init() {
	reader_c1_webui_catalog = pkg.CreateKafkaReader(pkg.C1WebuiCatalog)
	writer_c1_catalog_webui = pkg.CreateKafkaWriter(pkg.C1CatalogWebui)
}

func main() {
	log := logrus.New()
	log.Level = logrus.InfoLevel
	log.Out = os.Stdout

	s := server{}
	k := kafka_server{}
	go k.kafka_c1_webui_catalog()
	// go k.kafka_c2_webui_catalog()

	r := gin.Default()
	r.GET("/api/detail/payment", s.DetailPaymentHandler)
	r.GET("/api/detail/recommend", s.DetailRecommendHandler)
	r.GET("/api/payment", s.PaymentHandler)

	r.GET("/api/health", pkg.HealthCheck)
	r.Run(":8080")
}
