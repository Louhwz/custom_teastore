package main

import (
	"os"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// var (
// 	consumer_c3 *kafka.Conn
// 	consumer_c5 *kafka.Conn
// 	consumer_c6 *kafka.Conn
// )

// func init() {
// 	consumer_c3 = pkg.CreateConsumerCoon()
// }

func main() {
	log := logrus.New()
	log.Level = logrus.InfoLevel
	log.Out = os.Stdout

	s := server{}

	r := gin.Default()

	r.GET("/api/recommend", s.RecommendHandler)
	r.GET("/api/health", pkg.HealthCheck)
	r.Run(":8080")
}
