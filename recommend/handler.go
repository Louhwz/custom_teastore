package main

import (
	"net/http"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
)

type server struct {
}

func (s server) RecommendHandler(c *gin.Context) {
	pkg.PI(pkg.LargeLength)
	pkg.Memory(pkg.MemoryMedium)
	pkg.PI(pkg.SmallLength)
	c.String(http.StatusOK, pkg.GenerateString())
}
