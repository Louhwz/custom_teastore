package pkg

import (
	"context"
	"log"
	"time"

	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

const (
	HostConsumer = "kafka.kafka.svc.cluster.local:9092"

	HostProducer = "kafka-0.kafka-headless.kafka.svc.cluster.local:9092"
)

func CreateConsumerCoon(topic string) *kafka.Conn {
	// to produce messages
	// to consume messages
	partition := 0

	conn, err := kafka.DialLeader(context.Background(), "tcp", HostConsumer, topic, partition)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}

	conn.SetReadDeadline(time.Now().Add(10 * time.Second))
	return conn
}

func CreateProducerCoon(topic string) *kafka.Conn {
	// to produce messages
	partition := 0

	conn, err := kafka.DialLeader(context.Background(), "tcp", HostProducer, topic, partition)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}

	conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
	return conn
}

func CreateKafkaReader(topic string) *kafka.Reader {
	// make a new reader that consumes from topic-A, partition 0, at offset 42
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   []string{HostConsumer},
		Topic:     topic,
		Partition: 0,
		MinBytes:  10e3, // 10KB
		MaxBytes:  10e6, // 10MB
	})
	r.SetOffset(kafka.LastOffset)
	return r
}

func CreateKafkaWriter(topic string) *kafka.Writer {
	w := &kafka.Writer{
		Addr:     kafka.TCP(HostProducer),
		Topic:    topic,
		Balancer: &kafka.LeastBytes{},
		Async:    false,
	}
	return w
}

func CreateKafkaConsumerGroup(topic string) *kafka.Reader {
	// make a new reader that consumes from topic-A
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{HostConsumer},
		GroupID:  topic,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
	return r
}

func WriterMessage(w *kafka.Writer, key, value string) error {
	err := w.WriteMessages(context.Background(), kafka.Message{Key: []byte(key), Value: []byte(value)})
	if err != nil {
		logrus.Error("failed to write messages:", err)
		return err
	}
	return nil
}
