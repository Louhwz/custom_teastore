package pkg

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"math"
	"math/big"
	"math/rand"
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	SmallLength  = 20
	MediumLength = 70
	LargeLength  = 140
)

func PI(accuracy uint) *big.Float {
	k := 0
	pi := new(big.Float).SetPrec(accuracy).SetFloat64(0)
	k1k2k3 := new(big.Float).SetPrec(accuracy).SetFloat64(0)
	k4k5k6 := new(big.Float).SetPrec(accuracy).SetFloat64(0)
	temp := new(big.Float).SetPrec(accuracy).SetFloat64(0)
	minusOne := new(big.Float).SetPrec(accuracy).SetFloat64(-1)
	total := new(big.Float).SetPrec(accuracy).SetFloat64(0)

	two2Six := math.Pow(2, 6)
	two2SixBig := new(big.Float).SetPrec(accuracy).SetFloat64(two2Six)
	for {
		if k > int(accuracy) {
			break
		}
		t1 := float64(1) / float64(10*k+9)
		k1 := new(big.Float).SetPrec(accuracy).SetFloat64(t1)
		t2 := float64(64) / float64(10*k+3)
		k2 := new(big.Float).SetPrec(accuracy).SetFloat64(t2)
		t3 := float64(32) / float64(4*k+1)
		k3 := new(big.Float).SetPrec(accuracy).SetFloat64(t3)
		k1k2k3.Sub(k1, k2)
		k1k2k3.Sub(k1k2k3, k3)

		t4 := float64(4) / float64(10*k+5)
		k4 := new(big.Float).SetPrec(accuracy).SetFloat64(t4)
		t5 := float64(4) / float64(10*k+7)
		k5 := new(big.Float).SetPrec(accuracy).SetFloat64(t5)
		t6 := float64(1) / float64(4*k+3)
		k6 := new(big.Float).SetPrec(accuracy).SetFloat64(t6)
		k4k5k6.Add(k4, k5)
		k4k5k6.Add(k4k5k6, k6)
		k4k5k6 = k4k5k6.Mul(k4k5k6, minusOne)
		temp.Add(k1k2k3, k4k5k6)

		k7temp := new(big.Int).Exp(big.NewInt(-1), big.NewInt(int64(k)), nil)
		k8temp := new(big.Int).Exp(big.NewInt(1024), big.NewInt(int64(k)), nil)

		k7 := new(big.Float).SetPrec(accuracy).SetFloat64(0)
		k7.SetInt(k7temp)
		k8 := new(big.Float).SetPrec(accuracy).SetFloat64(0)
		k8.SetInt(k8temp)

		t9 := float64(256) / float64(10*k+1)
		k9 := new(big.Float).SetPrec(accuracy).SetFloat64(t9)
		k9.Add(k9, temp)
		total.Mul(k9, k7)
		total.Quo(total, k8)
		pi.Add(pi, total)

		k = k + 1
	}
	pi.Quo(pi, two2SixBig)
	return pi
}

const (
	MemorySmall  = 512 * 1024
	MemoryMedium = 5 * 1024 * 1024
	MemoryLarge  = 15 * 1024 * 1024
)

func Memory(size int) {
	k := make([]int, 0, size)
	for i := 0; i < size; i++ {
		k = append(k, i)
	}
}

type IOLEVEL int

const (
	IOSMALL  IOLEVEL = iota
	IOMEDIUM IOLEVEL = 1
	IOBIG    IOLEVEL = 2
)

func IO(level IOLEVEL, write bool) error {
	rand.Seed(time.Now().UnixNano())
	if write {
		file := fmt.Sprintf("/var/log/%v_%v.txt", time.Now(), rand.Int())
		err := createFileWithLongLine(file, level)
		if err != nil {
			logrus.Error(err)
		}
		return err
	} else {
		filePath := []string{
			"/lib/libz.so.1.2.11",
			"/lib/ld-musl-x86_64.so.1",
			"/lib/libcrypto.so.1.1",
		}

		err := readFileWithReadLine(filePath[0])
		if err != nil {
			logrus.Error(err)
		}
		return err
	}
}

func readFileWithReadLine(fn string) (err error) {

	file, err := os.Open(fn)

	if err != nil {
		return err
	}
	defer file.Close()

	// Start reading from the file with a reader.
	reader := bufio.NewReader(file)

	for {
		var buffer bytes.Buffer

		var l []byte
		var isPrefix bool
		for {
			l, isPrefix, err = reader.ReadLine()
			buffer.Write(l)

			// If we've reached the end of the line, stop reading.
			if !isPrefix {
				break
			}

			// If we're just at the EOF, break
			if err != nil {
				break
			}
		}

		if err == io.EOF {
			break
		}
	}

	if err != io.EOF {
		fmt.Printf(" > Failed!: %v\n", err)
	} else {
		err = nil
	}
	return
}

func createFileWithLongLine(fn string, level IOLEVEL) (err error) {
	file, err := os.Create(fn)

	if err != nil {
		return err
	}
	defer os.Remove(fn)

	w := bufio.NewWriter(file)

	size := []int{
		1024 * 512,
		1024 * 1024 * 5,
		1024 * 1024 * 15,
	}

	fs := size[level] // 4MB

	// Create a 4MB long line consisting of the letter a.
	for i := 0; i < fs; i++ {
		w.WriteRune('a')
	}

	// Terminate the line with a break.
	w.WriteRune('\n')

	// Put in a second line, which doesn't have a linebreak.
	w.WriteString("Second line.")

	w.Flush()

	return
}

func URL(service string, uri string) string {
	return fmt.Sprintf("http://%v:%v/%v", service, 8080, uri)
}
