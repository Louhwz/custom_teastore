package pkg

import (
	"math/rand"
	"time"
)

const (
	PAYMENT   = "payment"
	CATALOG   = "catalog"
	DETAIL    = "detail"
	RECOMMEND = "recommend"
	CART      = "cart"
	WEBUI     = "webui"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func init() {
	rand.Seed(time.Now().UnixNano())
}
func GenerateString() string {
	n := rand.Intn(500) + 500

	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
