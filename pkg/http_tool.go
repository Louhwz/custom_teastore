package pkg

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func FillResp(c *gin.Context, resp *http.Response, err error) {
	if err != nil {
		logrus.Error(fmt.Sprintf("施加error惩罚. error=%v", err))
		time.Sleep(1 * time.Second)
		c.String(http.StatusInternalServerError, err.Error())
	}
	if resp.StatusCode != http.StatusOK {
		logrus.Error(fmt.Sprintf("施加非200惩罚. error=%v", err))
		time.Sleep(1 * time.Second)
		c.String(http.StatusInternalServerError, fmt.Sprintf("not 200. code=%v. str=%v", resp.StatusCode, GenerateString()))
	} else {
		c.String(http.StatusOK, GenerateString())
	}
}
