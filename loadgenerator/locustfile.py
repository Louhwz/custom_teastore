import time
from locust import HttpUser, task, between


class QuickstartUser(HttpUser):
    wait_time = between(2, 3)
    G_URL = "192.168.0.97:30602"

    host = "http://{}".format(G_URL)

    @task(5)
    def catalog(self):
        self.client.get("/teastore/catalog/payment")

    @task(2)
    def catalog_detail_payment(self):
        self.client.get("/teastore/catalog/detail/payment")

    @task(3)
    def catalog_detail_recommend(self):
        self.client.get("/teastore/catalog/detail/recommend")

    @task(3)
    def cart_detail_payment(self):
        self.client.get("/teastore/cart/detail/payment")

    @task(2)
    def cart_detail_recommend(self):
        self.client.get("/teastore/cart/detail/recommend")

    @task(5)
    def cart_recommend(self):
        self.client.get("/teastore/cart/recommend")

    def on_start(self):
        pass
