import time
from locust import HttpUser, task, between


class QuickstartUser(HttpUser):
    wait_time = between(2, 3)
    G_URL = "10.103.70.140:8080"

    host = "http://{}".format(G_URL)

    @task(5)
    def catalog(self):
        self.client.get("/kafka/teastore/catalog")

    def on_start(self):
        pass
