package main

import (
	"os"

	"gitee.com/Louhwz/custom_teastore/pkg"

	"github.com/gin-gonic/gin"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

var (
	consumer_catalog_c2_detail *kafka.Conn
	producer_catalog_c2_detail *kafka.Conn
	consumer_detail_c2_payment *kafka.Conn
	producer_detail_c2_payment *kafka.Conn

	consumer_catalog_c3_detail   *kafka.Conn
	producer_catalog_c3_detail   *kafka.Conn
	consumer_detail_c3_recommend *kafka.Conn
	producer_detail_c3_payment   *kafka.Conn

	consumer_c3 *kafka.Conn
	consumer_c5 *kafka.Conn
	consumer_c6 *kafka.Conn
)

func init() {

}

func main() {
	log := logrus.New()
	log.Level = logrus.InfoLevel
	log.Out = os.Stdout

	s := server{}

	r := gin.Default()

	r.GET("/api/recommend", s.RecommendHandler)
	r.GET("/api/payment", s.PaymentHandler)
	r.GET("/api/health", pkg.HealthCheck)
	r.Run(":8080")
}
