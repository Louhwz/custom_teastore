package main

import (
	"net/http"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type server struct {
}

func (s server) PaymentHandler(ctx *gin.Context) {
	err := pkg.IO(pkg.IOMEDIUM, true)
	if err != nil {
		logrus.Error(err)
		ctx.String(http.StatusInternalServerError, err.Error())
		return
	}
	pkg.PI(pkg.SmallLength)
	pkg.Memory(pkg.MemorySmall)
	ctx.String(http.StatusOK, pkg.GenerateString())
}
