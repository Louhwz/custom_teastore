package main

import (
	"os"

	"gitee.com/Louhwz/custom_teastore/pkg"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func main() {
	log := logrus.New()
	log.Level = logrus.InfoLevel
	log.Out = os.Stdout

	s := server{}

	r := gin.Default()
	r.GET("/api/payment", s.PaymentHandler)
	r.GET("/api/health", pkg.HealthCheck)
	r.Run(":8080")
}
