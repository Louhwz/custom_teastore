package main

import (
	"context"
	"net/http"
	"time"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

type kafka_server struct{}

func (k kafka_server) kafka_c1_webui_catalog(c *gin.Context) {
	// uuid generator
	// do something
	// read until channel send msg
	// return
	msg_key := uuid.NewString()
	webuiThings()
	// 先实现一个无锁的版本吧
	now := time.Now()
	pkg.WriterMessage(writer_c1_webui_catalog, msg_key, "ok")
	logrus.Infof("write message at %v, spent=%v. msg_key=%v", time.Now().UnixNano(), time.Since(now), msg_key)
	// for {

	// 	m, err := reader_c1_webui_catalog.ReadMessage(context.Background())
	// 	if err != nil {
	// 		logrus.Error(err)
	// 	}
	// 	logrus.Infof("message at offset %d: %s = %s\n", m.Offset, string(m.Key), string(m.Value))
	// 	if string(m.Key) != msg_key {
	// 		continue
	// 	}
	// 	logrus.Info("命中! msg_key:", msg_key)
	// 	break
	// }
	for {
		now = time.Now()
		m, err := reader_c1_webui_catalog.ReadMessage(context.Background())
		if err != nil {
			logrus.Error(time.Since(now), err)
			return
		}
		if string(m.Key) != msg_key {
			logrus.Infof("从消息队列中得到了不一致的key id=%v", string(m.Key))
			continue
		}
		logrus.Infof("message spent=%v, at offset %d: %s = %s\n", time.Since(now), m.Offset, string(m.Key), string(m.Value))
		break
	}
	c.String(http.StatusOK, pkg.GenerateString())
}
