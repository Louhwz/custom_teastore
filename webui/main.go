package main

import (
	"os"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

var (
	// every instance should be consumer
	// consumer_webui_catalog *kafka.Conn
	// producer_webui_catalog *kafka.Conn

	// every instance should be consumer
	reader_c1_webui_catalog *kafka.Reader
	writer_c1_webui_catalog *kafka.Writer
)

func init() {
	reader_c1_webui_catalog = pkg.CreateKafkaReader(pkg.C1CatalogWebui)
	writer_c1_webui_catalog = pkg.CreateKafkaWriter(pkg.C1WebuiCatalog)
	// reader_c1_webui_catalog = pkg.CreateKafkaConsumerGroup(pkg.C1CatalogWebui)
}

func main() {
	log := logrus.New()
	log.Level = logrus.InfoLevel
	log.Out = os.Stdout

	s := server{}
	k := kafka_server{}
	r := gin.Default()

	// go KafkaConsumer()

	r.GET("/teastore/catalog/payment", s.CatalogPayment)
	r.GET("/teastore/catalog/detail/payment", s.CatalogDetailPayment)
	r.GET("/teastore/catalog/detail/recommend", s.CatalogDetailRecommend)
	r.GET("/teastore/cart/detail/payment", s.CartDetailPayment)
	r.GET("/teastore/cart/detail/recommend", s.CartDetailRecommend)
	r.GET("/teastore/cart/recommend", s.CartRecommend)

	// kafka group
	r.GET("/kafka/teastore/catalog", k.kafka_c1_webui_catalog)

	r.GET("/teastore/api/health", pkg.HealthCheck)

	r.Run(":8080")
}
