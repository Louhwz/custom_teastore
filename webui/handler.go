package main

import (
	"net/http"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
)

type server struct {
}

func webuiThings() {
	pkg.PI(pkg.SmallLength)
	pkg.Memory(pkg.MemorySmall)
	pkg.IO(pkg.IOSMALL, false)
}

func (s server) CatalogPayment(c *gin.Context) {
	webuiThings()
	resp, err := http.Get(pkg.URL(pkg.CATALOG, "api/payment"))
	pkg.FillResp(c, resp, err)
}

func (s server) CatalogDetailPayment(c *gin.Context) {
	webuiThings()
	resp, err := http.Get(pkg.URL(pkg.CATALOG, "api/detail/payment"))
	pkg.FillResp(c, resp, err)
}

func (s server) CatalogDetailRecommend(c *gin.Context) {
	webuiThings()
	pkg.Memory(pkg.MemoryMedium)
	resp, err := http.Get(pkg.URL(pkg.CATALOG, "api/detail/recommend"))
	pkg.FillResp(c, resp, err)
}

func (s server) CartDetailPayment(c *gin.Context) {
	webuiThings()
	resp, err := http.Get(pkg.URL(pkg.CART, "api/detail/payment"))
	pkg.FillResp(c, resp, err)
}

func (s server) CartDetailRecommend(c *gin.Context) {
	webuiThings()
	resp, err := http.Get(pkg.URL(pkg.CART, "api/detail/recommend"))
	pkg.FillResp(c, resp, err)
}

func (s server) CartRecommend(c *gin.Context) {
	webuiThings()
	resp, err := http.Get(pkg.URL(pkg.CART, "api/recommend"))
	pkg.FillResp(c, resp, err)
}
