package main

import (
	"os"

	"gitee.com/Louhwz/custom_teastore/pkg"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func main() {
	log := logrus.New()
	log.Level = logrus.InfoLevel
	log.Out = os.Stdout

	s := server{}

	r := gin.Default()
	r.GET("/api/detail/payment", s.PayHandler)
	r.GET("/api/detail/recommend", s.DetailRecommendHandler)
	r.GET("/api/recommend", s.RecommendHandler)
	r.GET("/api/health", pkg.HealthCheck)

	r.Run(":8080")
}
