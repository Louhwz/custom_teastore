package main

import (
	"net/http"

	"gitee.com/Louhwz/custom_teastore/pkg"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type server struct {
}

func (s server) PayHandler(c *gin.Context) {
	err := pkg.IO(pkg.IOMEDIUM, false)
	if err != nil {
		logrus.Error(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	pkg.Memory(pkg.MemoryMedium)
	pkg.PI(pkg.MediumLength)

	resp, err := http.Get(pkg.URL(pkg.DETAIL, "api/payment"))
	pkg.FillResp(c, resp, err)
}

func (s server) RecommendHandler(c *gin.Context) {
	err := pkg.IO(pkg.IOMEDIUM, false)
	if err != nil {
		logrus.Error(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	pkg.PI(pkg.SmallLength)

	resp, err := http.Get(pkg.URL(pkg.RECOMMEND, "api/recommend"))
	pkg.FillResp(c, resp, err)

}
func (s server) DetailRecommendHandler(c *gin.Context) {
	err := pkg.IO(pkg.IOMEDIUM, false)
	if err != nil {
		logrus.Error(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	pkg.Memory(pkg.MemoryMedium)
	pkg.PI(pkg.MediumLength)

	resp, err := http.Get(pkg.URL(pkg.DETAIL, "api/recommend"))
	pkg.FillResp(c, resp, err)
}
